package com.dmarsentev;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{

    @Test
    public void twoSeven()
    {
        App app = new App();

        String testSubstring =  "1\nTwo\n3\nTwo\n5\nTwo\nSeven\nTwo\n9\nTwo\n11\nTwo\n13\nTwoSeven";
        String resultSubstring = app.twoSeven().substring(0,52);
        assertTrue(  testSubstring.equals(resultSubstring) );

        String specFuncResult = app.specFunction(2,5);
        String expectedspecFuncResult = "1440";
        assertTrue(  specFuncResult.equals(expectedspecFuncResult) );
    }

    @Test
    public void specFunc()
    {
        App app = new App();

        String specFuncResult = app.specFunction(2,5);
        String expectedspecFuncResult = "1440";
        assertTrue(  specFuncResult.equals(expectedspecFuncResult) );
    }


    @Test
    public void wordsFrequencies()
    {
        App app = new App();


        String text = "aa aa bbb bbb bbb bbb   ee! ee; :ee";

        String expectedFrequencies = "bbb=>4\nee=>3\naa=>2\n";
        String result = app.wordsFrequencies(text);
        assertEquals(result, expectedFrequencies);
    }


}
