package com.dmarsentev;

import java.math.BigInteger;
import java.util.*;

/*
* Тестовые задания
* */
public class App 
{

    private static BigInteger bigZero = new BigInteger("0");
    private static BigInteger bigOne = new BigInteger("1");

    public static void main( String[] args )
    {

        App app = new App();
        System.out.println( app.twoSeven() );
        System.out.println( app.specFunction(2,5) );

        String text = "aa aa bbb bbb bbb bbb e ec ce c? c! d. f fu g - ee! ee; :ee";
        System.out.println( app.wordsFrequencies(text) );

    }


    /*
    * 1. Написать программу, которая выводит числа от 1 до 100,
    * но вместо чисел кратных 2 нужно выводить строку Two,
    * вместо чисел кратных 7 - строку Seven,
    * вместо чисел кратных 2 и 7 - строку TwoSeven.
    *
    * @return Строка с требуемым выводом.
    * */

    public String twoSeven(){
        StringBuffer sb = new StringBuffer();
        for(int i = 1; i<= 100; i++){
            if( (0  == i%2) && (0  == i%7) ){ sb.append("TwoSeven\n"); continue;}
            if( 0  == i%2 ){ sb.append("Two\n"); continue;}
            if( 0  == i%7 ){ sb.append("Seven\n");continue;}
            sb.append(String.valueOf(i) + "\n");
        }
        return sb.toString();
    }

/*
*    2. Написать программу, вычисляющую для любых натуральных m и r,
*    таких что r ≤ m, значение функции
*    𝑓(𝑚,𝑟) =
*    𝑚! 𝑟!(𝑚 − 𝑟)!
*    @param r
*    @param r
*    @return Строка с требуемым выводом.
*/
    public String specFunction(int r, int m){
       if( r<= 0) return "Недопустимое значение r=" + String.valueOf(r) + ", r должно быть > 0\n";
       if( m<= 0) return "Недопустимое значение m=" + String.valueOf(r) + ", m должно быть > 0\n";
       if( r > m ){
           return "Ошибка: r > m, а должно быть r<=m. Сейчас r=" + String.valueOf(r) + ", m=" + String.valueOf(m) + "\n";
       }


       BigInteger mFactorial = factorial(new BigInteger(String.valueOf(m)));
       BigInteger rFactorial = factorial(new BigInteger(String.valueOf(r)));
       BigInteger m_minus_r_Factorial = factorial(new BigInteger(String.valueOf(m-r)));

       return mFactorial.multiply(rFactorial).multiply(m_minus_r_Factorial).toString();
    }

    private static  BigInteger factorial(BigInteger n){
        if(n.compareTo(bigZero)<0)
            throw new RuntimeException("Ошибка. В функцию вычисления факториала передано отрицательное число"
                    + n.toString()+ "\n");

        if( n.equals( bigZero ) ) return bigOne;

        return factorial(n.subtract(bigOne)).multiply(n);

     }


     /*
     * 3. На вход программе подается литературный текст.
     * Программа должна вывести список слов, встречающихся в тексте,
     * в котором для каждого слова указывается количество вхождений этого слова в текст,
     * а слова выводятся в порядке убывания частоты вхождения.
     *
     * @param text - Строка с входным текстом
     * @return Строка, содержащая вывод в два столбца: слово-частота
     */

     public String wordsFrequencies(String text){
         String cleared = text.replaceAll("\\?|\\!|\\.|\\:|\\;|\\,|\"|\'|\\-","");
         String [] tokens = cleared.split("\\s+");
         HashMap<String,Integer> frequencies = new HashMap<>();

         for(String s: Arrays.asList(tokens)){
             if (frequencies.containsKey(s)) {
                 frequencies.put(s, 1 + frequencies.get(s));
             } else {
                 frequencies.put(s, 1);
             }
         }

         TreeMap<String, Integer> sortedMap = new TreeMap<>();
         sortedMap = sortMapByValue(frequencies);


         StringBuffer sb = new StringBuffer();

         for (Map.Entry<String,Integer> entry: sortedMap.entrySet() ) {
             sb.append(entry.getKey());
             sb.append("=>");
             sb.append( String.valueOf(entry.getValue()) );
             sb.append("\n");
         }

         return sb.toString();
     }

    public static TreeMap<String, Integer> sortMapByValue(HashMap<String, Integer> map){
        Comparator<String> comparator = new ValueComparator(map);

        TreeMap<String, Integer> result = new TreeMap<String, Integer>(comparator);
        result.putAll(map);
        return result;
    }

    static class ValueComparator implements Comparator<String> {

        HashMap<String, Integer> map = new HashMap<String, Integer>();

        public ValueComparator(HashMap<String, Integer> map){
            this.map.putAll(map);
        }

        @Override
        public int compare(String s1, String s2) {
            if(map.get(s1) >= map.get(s2)){
                return -1;
            }else{
                return 1;
            }
        }
    }
}


